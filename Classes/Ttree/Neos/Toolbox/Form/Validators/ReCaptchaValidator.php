<?php
namespace Ttree\Neos\Toolbox\Form\Validators;

use TYPO3\Flow\Annotations as FLOW;
use TYPO3\Flow\Validation\Validator\AbstractValidator;

/**
 * ReCaptcha Validator
 *
 * @api
 */
class ReCaptchaValidator extends AbstractValidator {

	const VERIFY_URL_PATTERN = "https://www.google.com/recaptcha/api/siteverify";

	protected $supportedOptions = array(
		'siteKey' => array(NULL, 'API key for the ReCaptcha validation', 'string'),
	);

	/**
	 * Returns TRUE, if the given property is a correct ReCAPTCHA
	 *
	 * @param mixed $value The value that should be validated
	 * @return boolean TRUE if the value is valid, FALSE if an error occured
	 */
	public function isValid($value) {
		$response = trim($_POST['g-recaptcha-response']);
		if ($response === '') {
			$this->addError('Ètes-vous un robot ? Essayez à nouveau ...', 1434114924);
			return FALSE;
		}
		if (!isset($this->options['siteKey']) || trim($this->options['siteKey']) === '') {
			$this->addError('Missing siteKey, check your form element configuration ...', 1434114925);
			return FALSE;
		}
		if (!$this->verifyCaptcha($response)) {
			$this->addError('Vous être très certainement un robot! Essayez à nouveau ...', 1434114926);
			return FALSE;
		}

		return FALSE;
	}

	protected function verifyCaptcha($response) {
		$curl = curl_init(self::VERIFY_URL_PATTERN);
		curl_setopt($curl, CURLOPT_POST, TRUE);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($curl, CURLOPT_POSTFIELDS, [
			'secret' => $this->options['siteKey'],
			'response' => $response,
			'remoteip' => $_SERVER['REMOTE_ADDR']
		]);
		if (isset($_ENV['http_proxy']) && !empty($_ENV['http_proxy'])) {
			curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, TRUE);
			curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
			curl_setopt($curl, CURLOPT_PROXY, $_ENV['http_proxy']);
		}

		$curlData = curl_exec($curl);

		curl_close($curl);

		$result = json_decode($curlData, TRUE);
		if ($result['success'] === TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}
<?php
namespace Ttree\Neos\Toolbox\Application;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "Ttree.Neos.Toolbox".    *
 *                                                                        *
 *                                                                        */
use TYPO3\Surf\Application\TYPO3\Flow;

/**
 * A TYPO3 Neos application template
 * @TYPO3\Flow\Annotations\Proxy(false)
 */
class Neos extends Flow {

	/**
	 * The production context
	 * @var string
	 */
	protected $context = 'Production';

	/**
	 * The TYPO3 Flow major and minor version of this application
	 * @var string
	 */
	protected $version = '2.0';

	/**
	 * Constructor
	 */
	public function __construct($name = 'TYPO3 Flow', $repositoryUrl, $sitePackageKey) {
		parent::__construct($name);
		$this->options = array_merge($this->options, array(
			'composerCommandPath' => '/usr/local/bin/composer',
			'transferMethod' => 'rsync',
			'packageMethod' => 'git',
			'updateMethod' => NULL,
			'repositoryUrl' => $repositoryUrl,
			'sitePackageKey' => $sitePackageKey
		));
		$this->setDeploymentPath('/var/www/vhosts/public/www.' . trim($name));
		$this->setContext('Production');
	}

}
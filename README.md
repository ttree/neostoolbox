# ttree neos toolbox

## Requirements

- Neos CMS 2.0
  
## Mixins / DocumentWithTeaserImageMixin

Add a simple teaser image to the current document node.

### Usage

```
'Your.Package:Document':
  superTypes:
    'Ttree.Neos.Toolbox:DocumentWithTeaserImageMixin': TRUE
```

## Mixins / InlineEditableTitleMixin

Change document configuration to support inline editable title.

### Usage

```
'Your.Package:Document':
  superTypes:
    'Ttree.Neos.Toolbox:InlineEditableTitleMixin': TRUE
```

## Mixins / AddressMixin

Basic configuration to add contact and address informations to the given node.

### Usage

```
'Your.Package:Document':
  superTypes:
    'Ttree.Neos.Toolbox:ContactInformationMixin': TRUE
    'Ttree.Neos.Toolbox:PostalAddressMixin': TRUE
```

### GenericNodeTypePostprocessor

**This processor is experimental, and API can change at any time**

The ```GenericNodeTypePostprocessor``` can be used to extend node type, based on the current ```superTypes```. 

Currently the PostProcessor support a basic property overlay to inject state for Switzerland. With the following configuration
you should have the switzerland's region in the ```addressRegion``` property.

```
'Your.Package:Document':
  superTypes:
    'Ttree.Neos.Toolbox:ContactInformationMixin': TRUE
    'Ttree.Neos.Toolbox:PostalAddressMixin': TRUE
    'Ttree.Neos.Toolbox:SwitzerlandPostalAddressMixin': TRUE
```

In the future this processor will be able to use multiple preset to have per ex. Switzerland + France region.

Check the ```Settings.yaml``` to change the default configuration of the processor.